<p align="center"> <img alt="Space Station 14" width="500" height="154" src="https://codeberg.org/Sirena/Sirena_website/raw/commit/2251dfb284b514df7adce6c558ca92f7c18cfa10/static/images/logo.png" /></p>

Space Station 14 это ремейк SS13, который работает на собственном движке [Robust Toolbox](https://github.com/space-wizards/RobustToolbox), написанном на C#.

Это репозиторий сервера Sirena. Здесь публикуются актуальные изменения и в целом разные вещи ТОЛЬКО для данного проекта.

## Ссылки

[Наш Discord](https://discord.station14.ru) | [Наша Вики](https://wiki.station14.ru) | [Steam](https://store.steampowered.com/app/1255460/Space_Station_14/) | [Клиент без Steam](https://spacestation14.io/about/nightlies/) | [Основной репозиторий](https://github.com/space-wizards/space-station-14)

## Документация

На официальном сайте с [документацией](https://docs.spacestation14.io/) имеется вся необходимая информация о контенте SS14, движке, дизайне игры и многом другом. Также имеется много информации для начинающих разработчиков.

## Контрибьют

В случае если вы хотите добавить новый контент будет лучше, если сначала вы предложите его в [основной репозиторий](https://github.com/space-wizards/space-station-14) или обсудите его необходимость на нашем сервере [Discord](https://discord.station14.ru).

## Сборка

1. Склонируйте этот репозиторий локально
2. Запустите `RUN_THIS.py` для инициализации подмодулей и скачивания движка.
3. Скомпилируйте проект.

[Более подробная инструкция по запуску проекта.](https://docs.spacestation14.com/en/general-development/setup.html)

## Лицензия

Весь код репозитория лицензирован под [MIT](https://github.com/space-syndicate/space-station-14/blob/master/LICENSE.TXT).

Большинство ассетов лицензированы под [CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/), если не указано иное. Ассеты имеют свою лицензию и авторские права в файле метаданных. [Пример](https://github.com/space-syndicate/space-station-14/blob/master/Resources/Textures/Objects/Tools/crowbar.rsi/meta.json).

Обратите внимание, что некоторые ассеты лицензированы на некоммерческой основе [CC-BY-NC-SA 3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/) или аналогичной некоммерческой лицензией, и их необходимо удалить, если вы хотите использовать этот проект в коммерческих целях.

<!--## Проверки
[![status-badge](https://ci.codeberg.org/api/badges/Sirena/SS14-Sirena/status.svg)](https://ci.codeberg.org/Sirena/SS14-Sirena)-->

## Feedback/обратная связь
If you notice that any license is infringing, please report it immediately either on Neko Dar#8948 in discord, or at email sirena.server@mail.ru

Если вы заметили, что какая либо лицензия нарушает права, просьба немедленно сообщить об этом или в дискорд Neko Dar#8948, или на почту sirena.server@mail.ru
