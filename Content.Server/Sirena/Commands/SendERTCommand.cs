using Content.Server.Administration;
using Content.Server.Administration.Logs;
using Content.Server.AlertLevel;
using Content.Server.Audio;
using Content.Server.Chat.Systems;
using Content.Shared.Sirena.Prototypes;
using Content.Server.Station.Systems;
using Content.Shared.Administration;
using Content.Shared.Database;
using Robust.Server.GameObjects;
using Robust.Server.Maps;
using Robust.Server.Player;
using Robust.Shared.Audio;
using Robust.Shared.Console;
using Robust.Shared.Map;
using Robust.Shared.Player;
using Robust.Shared.Prototypes;
using System.Linq;
using System.Numerics;

namespace Content.Server.Sirena.Commands;

[AdminCommand(AdminFlags.Fun)]
public sealed class SendERTCommand : IConsoleCommand
{
    [Dependency] private readonly IMapManager _mapManager = default!;
    [Dependency] private readonly IAdminLogManager _adminLogger = default!;
    [Dependency] private readonly IEntitySystemManager _system = default!;
    [Dependency] private readonly IEntityManager _entManager = default!;
    [Dependency] private readonly IPlayerManager _playerManager = default!;

    public string Command => "sendert";
    public string Description => Loc.GetString("send-ert-description");
    public string Help => Loc.GetString("send-ert-help");
    public void Execute(IConsoleShell shell, string argStr, string[] args)
    {
        bool playAnnounce;
        var protos = IoCManager.Resolve<IPrototypeManager>();
        var player = shell.Player;

        if (player?.AttachedEntity == null) // Are we the server's console?
        {
            shell.WriteLine(Loc.GetString("shell-only-players-can-run-this-command"));
            return;
        }

        var ertType = protos.Index<ERTTypePrototype>(args[0]);

        switch (args.Length)
        {
            case 0:
                shell.WriteLine(Loc.GetString("send-ert-help"));
                return;

            case 1:
                playAnnounce = ertType.DefaultIsAnnounce;
                break;

            default:
                if (bool.TryParse(args[1].ToLower(), out var temp))
                {
                    playAnnounce = temp;
                }
                else
                {
                    shell.WriteError(Loc.GetString($"send-ert-truefalse-error"));
                    return;
                }
                break;
        }
        if (ertType.ForsedAnnounce)
            playAnnounce = ertType.DefaultIsAnnounce;

        #region Command's body
        if (ertType.IsLoadGrid) // Create grid & map
        {
            var mapId = _mapManager.CreateMap();
            _system.GetEntitySystem<MetaDataSystem>().SetEntityName(
                _mapManager.GetMapEntityId(mapId),
                Loc.GetString("sent-ert-map-name")
            );

            var girdOptions = new MapLoadOptions
            {
                Offset = new Vector2(0, 0),
                Rotation = Angle.FromDegrees(0)
            };
            _system.GetEntitySystem<MapLoaderSystem>().Load(mapId, ertType.GridPath, girdOptions);
        }

        if (playAnnounce) // Write announce & play audio
        {
            if (ertType.IsSetAlertLevel)
            {
                var stationUid = _system.GetEntitySystem<StationSystem>().GetOwningStation(player.AttachedEntity.Value);
                if (stationUid == null) //We are on station? 
                {
                    shell.WriteLine(Loc.GetString("sent-ert-invalid-grid"));
                    return;
                }
                _system.GetEntitySystem<AlertLevelSystem>().SetLevel(
                    stationUid.Value,
                    ertType.AlertLevelCode,
                    false, true, true, true
                );
            }

            if (ertType.IsPlayAudio)
            {
                var filter = Filter.Empty().AddAllPlayers(_playerManager);

                var audioOption = AudioParams.Default;
                audioOption = audioOption.WithVolume(ertType.Volume);

                _entManager.System<ServerGlobalSoundSystem>().PlayAdminGlobal(filter, ertType.AudioPath, audioOption, true);
            }

            _system.GetEntitySystem<ChatSystem>().DispatchGlobalAnnouncement(
                Loc.GetString($"{ertType.AnnouncementText}"),
                Loc.GetString($"{ertType.AnnouncerText}"),
                playSound: ertType.IsPlayAuidoFromAnnouncement,
                colorOverride: ertType.AnnounceColor
            );
        }
        #endregion

        _adminLogger.Add(LogType.Action, LogImpact.High, $"{player} send ERT. Type: {ertType.ToString()}");
    }

    public CompletionResult GetCompletion(IConsoleShell shell, string[] args)
    {
        if (args.Length == 1)
        {
            var options = IoCManager.Resolve<IPrototypeManager>()
                .EnumeratePrototypes<ERTTypePrototype>()
                .Select(proto => new CompletionOption(proto.ID, Loc.GetString(proto.HintText)));
            return CompletionResult.FromHintOptions(options, Loc.GetString("send-ert-hint-type"));
        }

        if (args.Length == 2)
        {
            var options = new CompletionOption[]
            {
                new("true", Loc.GetString("send-ert-hint-isannounce-true")),
                new("false", Loc.GetString("send-ert-hint-isannounce-false")),
            };
            return CompletionResult.FromHintOptions(options, Loc.GetString("send-ert-hint-isannounce"));
        }

        return CompletionResult.Empty;
    }
}
