using Content.Server.Polymorph.Systems;
using Content.Shared.Audio;
using Content.Shared.Chemistry.Reagent;
using Content.Shared.Polymorph;
using Content.Shared.Popups;
using Robust.Shared.Audio;
using Robust.Shared.Player;
using Robust.Shared.Prototypes;

namespace Content.Server.Sirena.Chemistry.ReagentEffects;

public sealed partial class PolymorphEffect : ReagentEffect
{
    [Dependency] private readonly IEntitySystemManager _system = default!;

    [DataField("polymorphId", required: true)]
    public ProtoId<PolymorphPrototype> PolymorphId = default!;

    [DataField("polymorphSound")]
    public SoundSpecifier? PolymorphSound;

    [DataField("polymorphMessage")]
    public string? PolymorphMessage;

    public override void Effect(ReagentEffectArgs args)
    {
        EntityUid? polyUid = _system.GetEntitySystem<PolymorphSystem>().PolymorphEntity(args.SolutionEntity, PolymorphId);

        if (PolymorphSound != null && polyUid != null)
            SoundSystem.Play(PolymorphSound.GetSound(), Filter.Pvs(polyUid.Value), polyUid.Value, AudioHelpers.WithVariation(0.2f));

        if (PolymorphMessage != null && polyUid != null)
            _system.GetEntitySystem<SharedPopupSystem>().PopupEntity(Loc.GetString(PolymorphMessage), polyUid.Value, polyUid.Value, PopupType.Large);
    }
    protected override string? ReagentEffectGuidebookText(IPrototypeManager prototype, IEntitySystemManager entSys)
    {
        var polymorphPrototype = prototype.Index(PolymorphId);

        return Loc.GetString("reagent-effect-guidebook-create-polymorph-reaction-effect",
            ("chance", Probability),
            ("entname", polymorphPrototype.Entity));
    }
}
