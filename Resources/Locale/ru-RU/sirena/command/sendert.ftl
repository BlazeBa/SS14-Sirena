# Command
send-ert-description = Вызывает ОБР заданного типа
send-ert-help = sendert <тип ОБР> (объявляют себя? По умолчанию true)

# Errors
send-ert-truefalse-error = Вторая переменная не соответствует ни true, ни false
send-ert-erttype-error = Обнаружен неизвестный отряд ОБР
sent-ert-invalid-grid = Вы находитесь не на станции. Для запуска команды ваш персонаж должен находится на гриде станции

# Map name
sent-ert-map-name = Сектор патруля


ert-send-default-announcer = Центральное командование

# ERT type: default
ert-send-default-announcement = Внимание! Мы получили запрос на отряд быстрого реагирования. Запрос одобрен. Отряд будет подготовлен и отправлен в кротчайшие сроки.

# ERT type: security
ert-send-security-announcement = Внимание! Мы получили запрос на отряд быстрого реагирования. Запрос одобрен. Отряд будет подготовлен и отправлен в кротчайшие сроки.

# ERT type: engineer
ert-send-engineer-announcement = Внимание! Мы получили запрос на отряд быстрого реагирования. Запрос одобрен. Отряд будет подготовлен и отправлен в кротчайшие сроки.

# ERT type: medical
ert-send-medical-announcement = Внимание! Мы получили запрос на отряд быстрого реагирования. Запрос одобрен. Отряд будет подготовлен и отправлен в кротчайшие сроки.

# ERT type: janitor
ert-send-janitor-announcement = Внимание! Мы получили запрос на отряд быстрого реагирования. Запрос одобрен. Отряд будет подготовлен и отправлен в кротчайшие сроки.

# ERT type: cbrn
ert-send-cbrn-announcement = Внимание! Мы получили запрос на отряд быстрого реагирования. Запрос одобрен. Отряд будет подготовлен и отправлен в кротчайшие сроки.

# ERT type: deathsquad
ert-send-deathsquad-announcement = Последняя команда Центральное командование. Просьба экипаж станции оставаться на своих местах. Ожидайте шаттл эвакуации.

# ERT type: denial
ert-send-denial-announcement = Внимание! Мы получили запрос на отряд быстрого реагирования. Запрос отклонён. Попытайтесь решить проблемы своими силами.


# Hints
send-ert-hint-type = Тип ОБР

send-ert-hint-type-default = Стандартный отряд
send-ert-hint-type-security = Отряд СБ ОБР 
send-ert-hint-type-engineer = Отряд инженеров ОБР
send-ert-hint-type-medical = Отряд медиков ОБР
send-ert-hint-type-janitor = Отряд уборщиков ОБР
send-ert-hint-type-cbrn = Отряд ОБХАЗ
send-ert-hint-type-deathsquad = Отряд Эскадрона смерти
send-ert-hint-type-denial = Отказ в вызове ОБР


send-ert-hint-isannounce = Объявляют себя?

send-ert-hint-isannounce-true = Да
send-ert-hint-isannounce-false = Нет
